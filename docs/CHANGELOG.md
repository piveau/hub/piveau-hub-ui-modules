## [1.0.3-develop.3](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.3-develop.2...v1.0.3-develop.3) (2022-07-13)


### Bug Fixes

* this is a test for semantic-release ([57125d1](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/57125d123c6648007fd454af1a5b7fa3169ebc21))

## [1.0.3-develop.2](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.3-develop.1...v1.0.3-develop.2) (2022-07-13)


### Bug Fixes

* substr replaced by substring and several safeguards implemented ([821f257](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/821f257a28b60124b6182a14cda162bacaf5f0e5))

## [1.0.3-develop.1](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.2...v1.0.3-develop.1) (2022-07-13)


### Bug Fixes

* showcatalogdetails watching simplified ([8bbb747](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/8bbb7478ce88f497c01d09007b354d7c56235af0))

## [1.0.2](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.1...v1.0.2) (2022-07-13)


### Bug Fixes

* activate most linting properties again ([5c366e2](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/5c366e2392f7b75f65b739988388c7860c08dc20))
* activated catalogue watcher ([91a3a0c](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/91a3a0c2688507a9c012b8d5abeb4e99fafc517d))
* add dpiStore ([5bc8a18](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/5bc8a18b90a38899b0f8346d3b0d8369240f015e))
* mutation of prop in FadingDistributionOverlay.vue ([f853f49](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/f853f49ec5cd5618abca94de2365ce5a3e63c43c))
* simplified catalog watching in DatasetsFacets.vue ([0e70be3](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/0e70be3c81ce287a72ba1b9d893ae08fe151e968))
* vue/no-unused-components linting active again ([979a75a](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/979a75a7db67f553c92df2d9eb31309907025f05))
* yasgui initialization ([72ec058](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/72ec058c704b80a96b1480ed80faa79cf14b1d8d))

## [1.0.2-develop.3](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.2-develop.2...v1.0.2-develop.3) (2022-07-13)


### Bug Fixes

* activated catalogue watcher ([91a3a0c](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/91a3a0c2688507a9c012b8d5abeb4e99fafc517d))

## [1.0.2-develop.2](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.2-develop.1...v1.0.2-develop.2) (2022-07-12)


### Bug Fixes

* activate most linting properties again ([5c366e2](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/5c366e2392f7b75f65b739988388c7860c08dc20))
* mutation of prop in FadingDistributionOverlay.vue ([f853f49](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/f853f49ec5cd5618abca94de2365ce5a3e63c43c))
* vue/no-unused-components linting active again ([979a75a](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/979a75a7db67f553c92df2d9eb31309907025f05))

## [1.0.2-develop.1](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.1...v1.0.2-develop.1) (2022-07-12)


### Bug Fixes

* add dpiStore ([5bc8a18](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/5bc8a18b90a38899b0f8346d3b0d8369240f015e))
* yasgui initialization ([72ec058](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/72ec058c704b80a96b1480ed80faa79cf14b1d8d))

## [1.0.1](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.0...v1.0.1) (2022-07-08)


### Bug Fixes

* fix website freezing after selecting quality facet ([1910139](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/19101398ddb08a10a2c7feae44e8a46fa83226a0))

# 1.0.0 (2022-07-08)


### Bug Fixes

* adjust catalog img sizes ([f812d12](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/f812d12ca423af1daeac608f395cf286456812b9))
* **demo-app:** use correct 'to' prop for PvDataInfoBox ([fd31989](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/fd319891ae1f30f518c6ba5408802e50da448bf3))
* display 'unknown' format badges and fix minor visual differences to original DataInfoBox ([9c226f6](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/9c226f673931d8b821a16277e5b4959666dc335d)), closes [/gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/2153#note_229297](https://gitlab.fokus.fraunhofer.de//gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/2153/issues/note_229297)
* improve styling to visually align better with original ([cd89422](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/cd89422baeec8a9a909a60d7cd95e270575fd662))
* **PvDataInfoBox:** fix catalog mode body image sizing ([0069763](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/0069763200f05eb4b8943568f97dda0035272771))
* **PvDataInfoBox:** optimize spacing for badges ([8d4c084](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/8d4c084eef8df5e7bd79abd2d9b489dd765f874d))
* **PvDataInfoBox:** restore no description available typesetting ([4e04d02](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/4e04d027697e2090661634841a324c7842131dd8))
* swap updated and created blocks ([0bd35a2](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/0bd35a21a1c456649f069476b684dff505fc5e86))
* use correct format param object shape ([9000b6b](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/9000b6b66fd2bb6afbe4db02cc88b4622df56d55))


### Features

* add robots 'index' tag to DatasetDetails page ([167e555](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/167e555fc1d435c9ad9ff60a611a4f038e721f60))
* **pencil:** add 'Feature Release test' test to the tiltle for test, pls ignore ([680583d](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/680583dccab76a1f077ebf4b0477296b80947a52))
* **pencil:** added 'Feature Release test' test to the title for test, pls ignore ([705b4fa](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/705b4faeb046c5c22fac0e3bcf4629020f1f86f7))

# [1.0.0-develop.3](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.0-develop.2...v1.0.0-develop.3) (2022-07-08)


### Features

* **pencil:** added 'Feature Release test' test to the title for test, pls ignore ([705b4fa](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/705b4faeb046c5c22fac0e3bcf4629020f1f86f7))

# [1.0.0-develop.2](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/compare/v1.0.0-develop.1...v1.0.0-develop.2) (2022-07-08)


### Features

* **pencil:** add 'Feature Release test' test to the tiltle for test, pls ignore ([680583d](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/680583dccab76a1f077ebf4b0477296b80947a52))

# 1.0.0-develop.1 (2022-07-07)


### Bug Fixes

* adjust catalog img sizes ([f812d12](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/f812d12ca423af1daeac608f395cf286456812b9))
* **demo-app:** use correct 'to' prop for PvDataInfoBox ([fd31989](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/fd319891ae1f30f518c6ba5408802e50da448bf3))
* display 'unknown' format badges and fix minor visual differences to original DataInfoBox ([9c226f6](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/9c226f673931d8b821a16277e5b4959666dc335d)), closes [/gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/2153#note_229297](https://gitlab.fokus.fraunhofer.de//gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/2153/issues/note_229297)
* improve styling to visually align better with original ([cd89422](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/cd89422baeec8a9a909a60d7cd95e270575fd662))
* **PvDataInfoBox:** fix catalog mode body image sizing ([0069763](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/0069763200f05eb4b8943568f97dda0035272771))
* **PvDataInfoBox:** optimize spacing for badges ([8d4c084](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/8d4c084eef8df5e7bd79abd2d9b489dd765f874d))
* **PvDataInfoBox:** restore no description available typesetting ([4e04d02](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/4e04d027697e2090661634841a324c7842131dd8))
* swap updated and created blocks ([0bd35a2](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/0bd35a21a1c456649f069476b684dff505fc5e86))
* use correct format param object shape ([9000b6b](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/9000b6b66fd2bb6afbe4db02cc88b4622df56d55))


### Features

* add robots 'index' tag to DatasetDetails page ([167e555](https://gitlab.fokus.fraunhofer.de/piveau/hub/piveau-hub-ui-modules/commit/167e555fc1d435c9ad9ff60a611a4f038e721f60))
